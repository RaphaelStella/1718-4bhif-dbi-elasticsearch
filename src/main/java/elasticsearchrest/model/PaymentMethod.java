package elasticsearchrest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentMethod extends AbstractPersistable {

    private PaymentTypes paymentTypes;
    private Person owner;

    //This would be the bank address, credit card number, etc.
    private String paymentAddress;
    private LocalDate validUntil;

    //Bank name, Credit Card vendor, etc.
    private String vendor;

    //Usercode, email, etc.
    private String userLogin;

    //Pin, Paypal password, etc.
    private String userSecretLogin;
}
