package elasticsearchrest.model;

import org.joda.money.Money;
import org.joda.time.LocalDate;

import java.util.Collections;
import java.util.List;

public class Delivery extends AbstractPersistable {

    private Person person;
    private List<Product> products;
    private PaymentMethod paidWith;
    private LocalDate dateOrdered;
    private LocalDate arrivalDate;

    public Money calculateSum(){
        Money sum = Money.parse("0 EUR");
        for (Product p:products) {
            sum.plus(p.getPrice());
        }
        return sum;
    }

    public List<Product> getProducts() {
        return Collections.unmodifiableList(products);
    }
}
