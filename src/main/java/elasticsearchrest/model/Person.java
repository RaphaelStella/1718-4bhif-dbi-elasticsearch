package elasticsearchrest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.LocalDate;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Person extends AbstractPersistable{

    private String firstName;
    private String lastName;
    private String zipCode;
    private String street;
    private String country;
    private String email;
    private LocalDate dateOfBirth;
    private String password;

    private List<PaymentMethod> paymentMethods;
    private List<Delivery> deliveries;

    public List<PaymentMethod> getPaymentMethods(){
        return Collections.unmodifiableList(paymentMethods);
    }

    public List<Delivery> getDeliveries(){
        return Collections.unmodifiableList(deliveries);
    }

}
