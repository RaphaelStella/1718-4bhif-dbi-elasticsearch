package elasticsearchrest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.money.Money;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product extends AbstractPersistable {

    private String name;
    private String description;
    private Money price;

}
