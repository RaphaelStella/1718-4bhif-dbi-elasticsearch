package elasticsearchrest.model;

public enum PaymentTypes {
    PAYPAL, BANKACCOUNT, CREDITDCARD, SOFORT
}
