package elasticsearchrest.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@AllArgsConstructor
public abstract class AbstractDao<T> {

    private Class<T> persistable;

    @Setter
    @Getter
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    public List<T> findAllDocumentsOfIndex(String index){

        return null;
    }
}
